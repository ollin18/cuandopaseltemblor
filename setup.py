from setuptools import setup, find_packages

from os import path

"""
cuandopaseltemblor-pipeline: Pipeline cuandopaseltemblor
"""

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


setup(
    name="cuandopaseltemblor",
    version='0.0.2',
    description='Pipeline cuandopaseltemblor',
    long_description=long_description,
    url='https://github.com/ollin18/pipeline-template',
    author='Adolfo De Unánue',
    author_email='nanounanue@gmail.com',
    maintainer='Ollin Demian',
    maintainer_email='ollin.langle@ciencias.unam.mx',
    license='GPL v3',

    packages=find_packages(),

    test_suite='tests',

    install_requires=[
        'numpy',
        'pyyaml',
        'pandas',
        'scikit-learn',
        'sqlalchemy',
        'datetime',
        'scipy',
        'luigi',
        'Click',
        'python-dotenv'
    ],

    extra_require={
        'test': ['mock']
    },

    entry_points = {
        'console_scripts' : [
            'cuandopaseltemblor = cuandopaseltemblor.scripts.cli:main',

        ]
    },

    zip_safe=False
)
