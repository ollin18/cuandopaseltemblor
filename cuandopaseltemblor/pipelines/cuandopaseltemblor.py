# coding: utf-8
"""
cuandopaseltemblor Pipeline

.. module:: cuandopaseltemblor

   :synopsis: cuandopaseltemblor pipeline

.. moduleauthor:: Adolfo De Unánue <nanounanue@gmail.com>
"""

import os

import subprocess

import pandas as pd

import csv

import datetime

import luigi
import luigi.contrib.s3
import luigi.contrib.gcs

import sqlalchemy

## Variables de ambiente
from dotenv import load_dotenv, find_dotenv
load_dotenv(find_dotenv())


## Logging
import cuandopaseltemblor.config_ini

import logging

logger = logging.getLogger("dpa-template.cuandopaseltemblor")


import cuandopaseltemblor.pipelines.utils
import cuandopaseltemblor.pipelines.common



class cuandopaseltemblorPipeline(luigi.WrapperTask):
    """
    Task principal para el pipeline
    """

    def requires(self):
        yield PySparkTask()

class RTask(luigi.Task):

    root_path = luigi.Parameter()

    def requires(self):
        return RawData()

    def run(self):
        cmd = '''
              docker run --rm --network cuandopaseltemblor_net -v cuandopaseltemblor_store:/cuandopaseltemblor/data  cuandopaseltemblor/test-r
        '''

        logger.debug(cmd)

        out = subprocess.check_output(cmd, shell=True)

        logger.debug(out)

    def output(self):
        return luigi.LocalTarget(os.path.join(os.getcwd(), "data", "hola_mundo_desde_R.psv"))


class PythonTask(luigi.Task):

    def requires(self):
        return RTask()

    def run(self):
        cmd = '''
              docker run --rm --network cuandopaseltemblor_net  -v cuandopaseltemblor_store:/cuandopaseltemblor/data  cuandopaseltemblor/test-python --inputfile {} --outputfile {}
        '''.format(os.path.join("/cuandopaseltemblor/data", os.path.basename(self.input().path)),
                   os.path.join("/cuandopaseltemblor/data", os.path.basename(self.output().path)))

        logger.debug(cmd)

        out = subprocess.call(cmd, shell=True)

        logger.debug(out)

    def output(self):
        return luigi.LocalTarget(os.path.join(os.getcwd(), "data", "hola_mundo_desde_python.json"))


class PySparkTask(luigi.Task):
    def requires(self):
        return PythonTask()

    def run(self):
        cmd = '''
              docker run --rm --network cuandopaseltemblor_net -v cuandopaseltemblor_store:/cuandopaseltemblor/data cuandopaseltemblor/test-pyspark --master {} --input {} --output {}
        '''.format("spark://master:7077",
                   os.path.join("/cuandopaseltemblor/data", os.path.basename(self.input().path)),
                   os.path.join("/cuandopaseltemblor/data", os.path.basename(self.output().path)))

        logger.debug(cmd)

        out = subprocess.call(cmd, shell=True)

        logger.debug(out)

    def output(self):
        return luigi.LocalTarget(os.path.join(os.getcwd(), "data", "hola_mundo_desde_pyspark"))

class SqoopTask(luigi.Task):
    def requires(self):
        return HadoopTask()

    def run(self):
        cmd = '''
                docker run --rm --network cuandopaseltemblor_net -v cuandopaseltemblor_store:/cuandopaseltemblor/data cuandopaseltemblor/test-pyspark --master {} --input {} --output {}
        '''

        logger.debug(cmd)

        out = subprocess.call(cmd, shell=True)

        logger.debug(out)

    def output(self):
        return luigi.LocalTarget("hola_mundo_desde_sqoop.txt")

class HadoopTask(luigi.Task):
    def requires(self):
        return PySparkTask()

    def run(self):
        cmd = '''
                docker run --rm --network cuandopaseltemblor_net -v cuandopaseltemblor_store:/cuandopaseltemblor/data cuandopaseltemblor/test-pyspark --master {} --input {} --output {}
        '''

        logger.debug(cmd)

        out = subprocess.call(cmd, shell=True)

        logger.debug(out)


    def output(self):
        return luigi.LocalTarget("hola_mundo_desde_hadoop.txt")

class RawData(luigi.ExternalTask):
    def output(self):
        return luigi.LocalTarget("./data/raw_data.txt")


