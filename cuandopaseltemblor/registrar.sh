#!/usr/bin/env bash
IMAGES="test-r test-python"
IMAGE_VERSION="0.1"
# DOCKER_REGISTRY="localhost:5000"

for IMAGE in ${IMAGES} ; do
    docker build --tag ollin18/${IMAGE}:${IMAGE_VERSION} tasks/${IMAGE}
    docker push ollin18/${IMAGE}:${IMAGE_VERSION}
done
