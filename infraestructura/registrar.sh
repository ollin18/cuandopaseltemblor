#!/usr/bin/env bash
IMAGES="base python r luigi-server java spark-client"
IMAGE_VERSION="0.1"
# DOCKER_REGISTRY="localhost:5000"

for IMAGE in ${IMAGES} ; do
    docker build --tag ollin18/${IMAGE}:${IMAGE_VERSION} ${IMAGE}
    docker push ollin18/${IMAGE}:${IMAGE_VERSION}
done
